//
//  ViewController.swift
//  radioPause
//
//  Created by Polumna on 22.10.16.
//  Copyright © 2016 Ilya Rudentsov. All rights reserved.
//
//Серебрянный дождь http://radiosilver.corbina.net:8000/silver128a.mp3

import UIKit
import AVKit
import MediaPlayer
import SafariServices

class ViewController: UIViewController {
    
    @IBOutlet weak var playButton: UIButton!
    var jukebox : Jukebox!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        jukebox = Jukebox(delegate: self, items: [JukeboxItem(URL: URL(string: "http://radiosilver.corbina.net:8000/silver128a.mp3")!)])!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func playTouched(_ sender: UIButton) {
        switch jukebox.state {
        case .ready :
            sender.setTitle("Pause", for: UIControlState.normal)
            jukebox.play(atIndex: 0)
        case .playing :
            sender.setTitle("Play", for: UIControlState.normal)
            jukebox.pause()
        case .paused :
            sender.setTitle("Pause", for: UIControlState.normal)
            jukebox.play()
        default:
            jukebox.stop()
        }
        
    }
    @IBAction func stopTouch(_ sender: UIButton) {
        playButton.setTitle("Play", for: UIControlState.normal)
        jukebox.stop()
    }
    
    @IBAction func openSiteTouch(_ sender: UIButton) {
        
        let safari = SFSafariViewController(url: URL(string: "http://www.silver.ru")!)
        //safari.modalPresentationStyle = .
        present(safari, animated: true, completion: nil)
    }
    

}

extension ViewController: JukeboxDelegate{
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        /*
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            //let value = Float(currentTime / duration)
            
            slider.value = value
            populateLabelWithTime(currentTimeLabel, time: currentTime)
            populateLabelWithTime(durationLabel, time: duration)
         
        } else {
    
            resetUI()
 
        }
        */
    }
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        /*
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.indicator.alpha = jukebox.state == .loading ? 1 : 0
            self.playPauseButton.alpha = jukebox.state == .loading ? 0 : 1
            self.playPauseButton.isEnabled = jukebox.state == .loading ? false : true
        })
        
        if jukebox.state == .ready {
            playPauseButton.setImage(UIImage(named: "playBtn"), for: UIControlState())
        } else if jukebox.state == .loading  {
            playPauseButton.setImage(UIImage(named: "pauseBtn"), for: UIControlState())
        } else {
            volumeSlider.value = jukebox.volume
            let imageName: String
            switch jukebox.state {
            case .playing, .loading:
                imageName = "pauseBtn"
            case .paused, .failed, .ready:
                imageName = "playBtn"
            }
            playPauseButton.setImage(UIImage(named: imageName), for: UIControlState())
        }*/
        
        print("Jukebox state changed to \(jukebox.state)")
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }
    
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                jukebox.play()
            case .remoteControlPause :
                jukebox.pause()
            case .remoteControlNextTrack :
                jukebox.playNext()
            case .remoteControlPreviousTrack:
                jukebox.playPrevious()
            case .remoteControlTogglePlayPause:
                if jukebox.state == .playing {
                    jukebox.pause()
                } else {
                    jukebox.play()
                }
            default:
                break
            }
        }
    }
    
}
